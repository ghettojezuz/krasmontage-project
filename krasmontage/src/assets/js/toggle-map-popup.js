export const toggleMapPopup = () => {
    const mapPopup = document.getElementById('map-popup');
    if (mapPopup.className.includes('active')) {
        mapPopup.classList.remove('active')
    } else {
        mapPopup.classList.add('active')
    }

    // CLOSE MAP IF CLICKED OUTSIDE
    if (mapPopup.className.includes('active')) {
        document.addEventListener('click', (event) => {
            if ([...event.target.classList].indexOf('map-popup') !== -1) {
                mapPopup.classList.remove('active');
            }
        });
    }

};