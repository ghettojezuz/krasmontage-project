import {openAgreePopup, closeAgreePopup, openSuccessPopup, closeSuccessPopup} from './popupTogglers';

// FORM ITEMS: name, phone, tons, cost, submitButton

export const setCapacityFormSubmit = () => {
    //1 - валидация, 2 - согласие, 3 - отправка
    const formValidation = (event) => {
        if (event.target.elements.name.value === "" ||
            event.target.elements.phone.value === "" ||
            event.target.elements.tons.value === "" ||
            event.target.elements.cost.value === "") {
            return false;
        } else {
            return true;
        }
    };

    const checkIsAgree = (event) => {
        return event.target.elements.agreeCheckbox.checked;
    };

    const capacityForms = [...document.getElementsByClassName('capacity__form')];
    capacityForms.forEach((capacityForm) => {
        capacityForm.addEventListener('submit', (event) => {
            let isValid = formValidation(event);
            if (isValid) {
                openAgreePopup();
                let agreeForm = document.getElementById('agree-form');

                // On agree form submit
                agreeForm.addEventListener('submit', (event) => {
                    event.preventDefault();
                    let isAgree = checkIsAgree(event);
                    if (isAgree) {
                        // Example of data
                        let data = {
                            name: capacityForm.elements.name.value,
                            phone: capacityForm.elements.phone.value,
                            tons: capacityForm.elements.tons.value,
                            cost: capacityForm.elements.cost.value,
                        };
                        let isSuccess;
                        // Data transfer to  server here, then set flag isSuccess
                        isSuccess = true;
                        if (isSuccess) {
                            closeAgreePopup();
                            openSuccessPopup();
                            let successForm = document.getElementById('success-form');
                            successForm.elements.submitButton.onclick = function (event) {
                                event.preventDefault();
                                closeSuccessPopup();
                            }
                        }
                    } else {
                        // If not agree
                    }
                });
            } else {
                // If form is invalid
                if (event.target.elements.name.value === "") {
                    let inputGroup = event.target.querySelector('.input-group--name');
                    inputGroup.classList.add('error');
                    event.target.elements.name.addEventListener('input', () => {
                        inputGroup.classList.remove('error');
                    })
                }
                if (event.target.elements.phone.value === "") {
                    let inputGroup = event.target.querySelector('.input-group--phone');
                    inputGroup.classList.add('error');
                    event.target.elements.phone.addEventListener('input', () => {
                        inputGroup.classList.remove('error');
                    })
                }
            }
        });
    });
};