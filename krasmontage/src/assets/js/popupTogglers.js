export const openAgreePopup = () => {
    let agreePopup = document.getElementById('agree-popup');
    agreePopup.classList.add('active');
};

export const closeAgreePopup = () => {
    let agreePopup = document.getElementById('agree-popup');
    agreePopup.classList.remove('active');
};

export const openSuccessPopup = () => {
    let agreePopup = document.getElementById('success-popup');
    agreePopup.classList.add('active');
};

export const closeSuccessPopup = () => {
    let agreePopup = document.getElementById('success-popup');
    agreePopup.classList.remove('active');
};

export const openRequestPopup = () => {
    let requestPopup = document.getElementById('request-popup');
    requestPopup.classList.add('active');
};

export const closeRequestPopup = () => {
    let requestPopup = document.getElementById('request-popup');
    requestPopup.classList.remove('active');
};