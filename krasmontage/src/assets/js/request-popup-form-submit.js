import {closeRequestPopup, openSuccessPopup, closeSuccessPopup} from './popupTogglers';

export const setRequestPopupFormSubmit = () => {
    const formValidation = (event) => {
        if (event.target.elements.name.value === "" ||
            event.target.elements.phone.value === "" ||
            event.target.elements.email.value === "" ||
            !event.target.elements.requestCheckbox.checked) {
            return false;
        } else {
            return true;
        }
    };

    const requestForm = document.getElementById('request-form');
    requestForm.addEventListener('submit', (event) => {
        event.preventDefault();
        const isValid = formValidation(event);
        if (isValid) {
            // Example of data
            let data = {
                name: requestForm.elements.name.value,
                phone: requestForm.elements.phone.value,
                email: requestForm.elements.email.value,
                comment: requestForm.elements.comment.value,
            };
            let isSuccess;
            // Data transfer to  server here, then set flag isSuccess
            isSuccess = true;
            if (isSuccess) {
                closeRequestPopup();
                openSuccessPopup();
                let successForm = document.getElementById('success-form');
                successForm.elements.submitButton.onclick = function (event) {
                    event.preventDefault();
                    closeSuccessPopup();
                }
            }
        } else {
            // If form is invalid
            if (event.target.elements.name.value === "") {
                let inputGroup = event.target.querySelector('.input-group--name');
                inputGroup.classList.add('error');
                event.target.elements.name.addEventListener('input', () => {
                    inputGroup.classList.remove('error');
                })
            }
            if (event.target.elements.phone.value === "") {
                let inputGroup = event.target.querySelector('.input-group--phone');
                inputGroup.classList.add('error');
                event.target.elements.phone.addEventListener('input', () => {
                    inputGroup.classList.remove('error');
                })
            }
            if (event.target.elements.email.value === "") {
                let inputGroup = event.target.querySelector('.input-group--email');
                inputGroup.classList.add('error');
                event.target.elements.email.addEventListener('input', () => {
                    inputGroup.classList.remove('error');
                })
            }
        }
    });

};