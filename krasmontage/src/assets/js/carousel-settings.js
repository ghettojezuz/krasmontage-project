import $ from 'jquery';

export const setCarousel = () => {
    const setManufactureCarousel = () => {
        var delay = 0,
            carouselItems = $('.manufacture-slide-1 .carousel__item'),
            step = 500 / carouselItems.length; /* 5 is the animation duration */
        carouselItems.each(function () {
            $(this).css('animation-delay', -delay + "s");
            delay += step;
        });
    };
    const setBuildingCarousel = () => {
        var delay = 0,
            carouselItems = $('.building-slide-1 .carousel__item'),
            step = 500 / carouselItems.length; /* 5 is the animation duration */
        carouselItems.each(function () {
            $(this).css('animation-delay', -delay + "s");
            delay += step;
        });
    };

    setManufactureCarousel();
    setBuildingCarousel();

};