export const setCookieConsent = () => {
    if (!localStorage.getItem('cookieconsent')) {
        setTimeout(() => {
            const cookieConsent = document.getElementById('cookie-consent');
            const cookieConsentButton = document.querySelector('.cookie-consent .button');
            cookieConsent.style.display = 'flex';
            cookieConsentButton.onclick = function (e) {
                e.preventDefault();
                cookieConsent.style.display = 'none';
                localStorage.setItem('cookieconsent', true);
            }
        }, 8000);

    }
};