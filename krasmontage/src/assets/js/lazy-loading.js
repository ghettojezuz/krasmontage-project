import LazyLoad from "vanilla-lazyload";

export const setLazyLoading = () => {
    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy",
        cancel_on_exit: true,
        threshold: 0,
    });
    lazyLoadInstance.update();
};