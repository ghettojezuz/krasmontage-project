export const setYandexMap = () => {
    ymaps.ready(init);
    function init(){
        var myMap = new ymaps.Map("ya-map", {
                center: [55.995779055966594,92.92098094047539],
                zoom: 15,
                behaviors: ['scrollZoom', 'drag'],
            }),

            myPlacemark = new ymaps.GeoObject({
                geometry: {
                    type: 'Point',
                    coordinates: [55.99578506875794,92.92107749999992],
                }
            }, {
                preset: 'islands#redDotIcon',
            });

        myMap.geoObjects.add(myPlacemark);
    };
};