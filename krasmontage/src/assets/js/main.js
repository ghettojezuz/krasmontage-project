import $ from 'jquery';
import {SwiperMain, SwiperManufacture} from "./swiper-settings";
import {setCarousel} from "./carousel-settings";
import {setSwiper} from "./swiper-settings";
import {setCapacity} from "./capaticy-settings";
import {setCookieConsent} from "./cookie-consent";
import {setCapacityFormSubmit} from "./capacity-form-submit";
import {toggleMenu} from "./toggle-menu";
import {toggleMapPopup} from "./toggle-map-popup";
import {closeAgreePopup,  closeSuccessPopup, openRequestPopup, closeRequestPopup} from './popupTogglers';
import {setRequestPopupFormSubmit} from "./request-popup-form-submit";
import {animateDecorLines} from "./animateDecorLines";
import {setYandexMap} from "./yandex-map-settings";
import {setLazyLoading} from "./lazy-loading";

const main = () => {

    // ----------------------- MAIN LOOP -----------------------

    animateDecorLines();
    setSwiper();
    setCarousel();
    setCapacity();
    setCapacityFormSubmit();
    setRequestPopupFormSubmit();
    setCookieConsent();

    // if (SwiperMain !== undefined) {
    //     SwiperMain.slideTo(2);
    // }
    //
    // if (SwiperManufacture !== undefined) {
    //     SwiperManufacture.slideTo(1);
    // }

    // -------------------- EVENT LISTENERS --------------------

    // RETURN TO 1ST SLIDE WHEN LOGO CLICKED
    const logo = document.getElementById('logo-link');
    logo.addEventListener('click', () => {
        SwiperMain.slideTo(0);
    });

    // OPEN TOGGLER FOR MOBILE MENU
    const togglerOpen = document.getElementById('toggler--open');
    togglerOpen.addEventListener('click', () => {
        toggleMenu();
    });

    // CLOSE TOGGLER FOR MOBILE MENU
    const togglerClose = document.getElementById('toggler--close');
    togglerClose.addEventListener('click', () => {
        toggleMenu();
    });

    //CLOSE MOBILE MENU ON MENU-LINK, LOGO AND BUTTON CLICK
    const menuItems = document.getElementsByClassName('pagination__item');
    const menuItemsArray = [...menuItems];
    menuItemsArray.forEach((item) => {
        item.addEventListener('click', () => {
            toggleMenu();
        })
    });
    const menuLogo = document.getElementById('logo--menu-mobile');
    menuLogo.addEventListener('click', () => {
        toggleMenu();
    });
    const menuButton = document.getElementById('button--menu-mobile');
    menuButton.addEventListener('click', () => {
        toggleMenu();
    });

    // TOGGLE MAP POPUP (OPEN)
    const mapTogglerOpen = document.getElementById('map-toggler--open');
    mapTogglerOpen.addEventListener('click', () => {
        toggleMapPopup();
    });

    // TOGGLE MAP POPUP (CLOSE)
    const mapTogglerClose = document.getElementById('map-toggler--close');
    mapTogglerClose.addEventListener('click', () => {
        toggleMapPopup();
    });

    // TOGGLE MAP POPUP ON MOBILE (OPEN)
    const mapMobileTogglerOpen = document.getElementById('map-mobile-toggler--open');
    mapMobileTogglerOpen.addEventListener('click', () => {
        toggleMapPopup();
    });

    // CLOSE TOGGLER FOR AGREE POPUP
    const agreePopupCloseToggler = document.getElementById('agree-form__close');
    agreePopupCloseToggler.addEventListener('click', () => {
        closeAgreePopup();
    });

    // CLOSE TOGGLER FOR SUCCESS POPUP
    const successPopupCloseToggler = document.getElementById('success-form__close');
    successPopupCloseToggler.addEventListener('click', () => {
        closeSuccessPopup();
    });

    // OPEN REQUEST POPUP
    const requestPopupOpenButton = document.getElementById('request-popup-open-button');
    requestPopupOpenButton.addEventListener('click', () => {
        openRequestPopup();
    });

    // OPEN REQUEST POPUP ON MOBILE
    const requestPopupOpenButtonMobile = document.getElementById('button--menu-mobile');
    requestPopupOpenButtonMobile.addEventListener('click', () => {
        openRequestPopup();
    });

    // CLOSE REQUEST POPUP
    const requestPopupCloseToggler = document.getElementById('request-form__close');
    requestPopupCloseToggler.addEventListener('click', () => {
        closeRequestPopup();
    });
};





setLazyLoading();
$(window).on('load', () => {
    const preload = document.getElementById('preload');
    preload.style.opacity = '0';

    setTimeout(() => {
        preload.style.display = 'none';
    }, 500);

    main();
    setYandexMap();
});

