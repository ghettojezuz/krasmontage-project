export const animateDecorLines = () => {
    let decorLines = document.getElementsByClassName('decor__line');
    for (let i = 0; i < decorLines.length; i++) {
        setTimeout(() => {
            decorLines[i].classList.add('active');
        }, (i * 300));
    }
};