import Swiper from 'swiper';
import {breakpoint} from "./breakpoint";

export let SwiperMain;
export let SwiperManufacture;
export let SwiperBuilding;
export let SwiperCardSm_Manufacture;
export let SwiperCardLg_Manufacture;
export let SwiperCardSm_Building;
export let SwiperCardLg_Building;
export let SwiperCoverflow_1;
export let SwiperCoverflow_2;
export let SwiperLicense;

export const setSwiper = () => {

    const setMainSwiper = () => {
        const menu = ['Первый слайд (IF YOU SEE THIS, YOU DOING SMTHING WRONG)',
            'Производство',
            'Выполненные объёмы',
            'Монтаж и строительство',
            'Выполненные объекты',
            'Сертификация',
            'Лаборатория',
            'Заключительный слайд (IF YOU SEE THIS, YOU DOING SMTHING WRONG)'];

        const enableSwiper = function() {
            SwiperMain = new Swiper('.swiper-main', {
                direction: 'vertical',
                effect: 'fade',
                speed: 400,
                fadeEffect: {
                    crossFade: true,
                },
                allowTouchMove: false,
                touchRatio: 0,
                mousewheel: true,
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return `<span class="${className}"><span class="swiper-pagination-bullet-text">${menu[index]}</span></span>`
                    }
                },
            });
        };

        const breakpointChecker = function() {
            if ( breakpoint.matches === true ) {
                if ( SwiperMain !== undefined ) SwiperMain.destroy( true, true );
                return;
            } else if ( breakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        breakpoint.addListener(breakpointChecker);
        breakpointChecker();
    };

    const setManufactureSwiper = () => {
        const enableSwiper = function() {
            SwiperManufacture = new Swiper('.swiper-manufacture', {
                direction: 'horizontal',
                touchRatio: 0,
                effect: 'fade',
                speed: 600,
                fadeEffect: {
                    crossFade: true,
                },
                slidesPerView: 1,
                navigation: {
                    nextEl: '.arrow-next',
                    prevEl: '.arrow-back',
                }
            });
        };

        const breakpointChecker = function() {
            if ( breakpoint.matches === true ) {
                if ( SwiperMain !== undefined ) SwiperMain.destroy( true, true );
                return;
            } else if ( breakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        breakpoint.addListener(breakpointChecker);
        breakpointChecker();
    };

    const setBuildingSwiper = () => {
        const enableSwiper = function() {
            SwiperBuilding = new Swiper('.swiper-building', {
                direction: 'horizontal',
                touchRatio: 0,
                effect: 'fade',
                speed: 400,
                fadeEffect: {
                    crossFade: true,
                },
                slidesPerView: 1,
                navigation: {
                    nextEl: '.arrow-next',
                    prevEl: '.arrow-back',
                }
            });
        };

        const breakpointChecker = function() {
            if ( breakpoint.matches === true ) {
                if ( SwiperMain !== undefined ) SwiperMain.destroy( true, true );
                return;
            } else if ( breakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        breakpoint.addListener(breakpointChecker);
        breakpointChecker();
    };

    const setCardSliderSm_Manufacture = () => {
        SwiperCardSm_Manufacture = new Swiper('.card-slider-sm--manufacture', {
            direction: 'horizontal',
            slidesPerView: 1.5,
            spaceBetween: 6,
            nested: true,
            speed: 1200,
            autoplay: {
                delay: 7000,
                disableOnInteraction: false,
            },
            breakpoints: {
                960: {
                    slidesPerView: 3,
                }
            }
        });
    };

    const setCardSliderLg_Manufacture = () => {
        SwiperCardLg_Manufacture = new Swiper('.card-slider-lg--manufacture', {
            direction: 'horizontal',
            slidesPerView: 1.2,
            spaceBetween: 6,
            nested: true,
            speed: 1200,
            autoplay: {
                delay: 6000,
                disableOnInteraction: false,
            },
            breakpoints: {
                960: {
                    slidesPerView: 2,
                }
            }
        });
    };

    const setCardSliderSm_Building = () => {
        SwiperCardSm_Manufacture = new Swiper('.card-slider-sm--building', {
            direction: 'horizontal',
            slidesPerView: 1.5,
            spaceBetween: 6,
            nested: true,
            speed: 1200,
            autoplay: {
                delay: 7000,
                disableOnInteraction: false,
            },
            breakpoints: {
                960: {
                    slidesPerView: 3,
                }
            }
        });
    };

    const setCardSliderLg_Building = () => {
        SwiperCardLg_Manufacture = new Swiper('.card-slider-lg--building', {
            direction: 'horizontal',
            slidesPerView: 1.2,
            spaceBetween: 6,
            nested: true,
            speed: 1200,
            autoplay: {
                delay: 6000,
                disableOnInteraction: false,
            },
            breakpoints: {
                960: {
                    slidesPerView: 2,
                }
            }
        });
    };

    const setCoverflowSwiper_1 = () => {
        SwiperCoverflow_1 = new Swiper('.coverflow-slider_1', {
            direction: 'horizontal',
            grabCursor: true,
            slidesPerView: 1.15,
            initialSlide: 2,
            centeredSlides: true,
            speed: 300,
            effect: 'coverflow',
            longSwipesMs: 5000,
            spaceBetween: 14,
            slideToClickedSlide: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 0,
            },
            scrollbar: {
                el: '.coverflow-slider-scrollbar',
            },
            navigation: {
                nextEl: '.coverflow-button-next',
                prevEl: '.coverflow-button-prev',
            },
            breakpoints: {
                960: {
                    spaceBetween: 0,
                    slidesPerView: 'auto',
                    // coverflowEffect: {
                    //     rotate: 0,
                    //     stretch: 150,
                    //     depth: 500,
                    // }
                    coverflowEffect: {
                        rotate: 10,
                        stretch: 50,
                        depth: 100,
                    },
                }
            }
        });
    };

    const setCoverflowSwiper_2 = () => {
        SwiperCoverflow_2 = new Swiper('.coverflow-slider_2', {
            direction: 'horizontal',
            grabCursor: true,
            slidesPerView: 1.15,
            initialSlide: 2,
            centeredSlides: true,
            speed: 300,
            effect: 'coverflow',
            longSwipesMs: 5000,
            spaceBetween: 14,
            slideToClickedSlide: true,
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 0,
            },
            scrollbar: {
                el: '.coverflow-slider-scrollbar',
            },
            navigation: {
                nextEl: '.coverflow-button-next',
                prevEl: '.coverflow-button-prev',
            },
            breakpoints: {
                960: {
                    spaceBetween: 0,
                    slidesPerView: 'auto',
                    // coverflowEffect: {
                    //     rotate: 0,
                    //     stretch: 150,
                    //     depth: 500,
                    // }
                    coverflowEffect: {
                        rotate: 10,
                        stretch: 50,
                        depth: 100,
                    },
                }
            }
        });
    };

    const setLicenseSlider = () => {
        const enableSwiper = function() {
            SwiperLicense = new Swiper('.license-slider', {
                direction: 'horizontal',
                initialSlide: 3,
                slidesPerView: 'auto',
                centeredSlides: true,
                nested: true,
                speed: 700,
                effect: 'coverflow',
                coverflowEffect: {
                    rotate: 0,
                    stretch: 100,
                    depth: 0,
                    slideShadows : true,
                },
                scrollbar: {
                    el: '.license-slider-scrollbar',
                },
            });
        };

        const breakpointChecker = function() {
            if ( breakpoint.matches === true ) {
                if ( SwiperMain !== undefined ) SwiperMain.destroy( true, true );
                return;
            } else if ( breakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        breakpoint.addListener(breakpointChecker);
        breakpointChecker();
    };


    setMainSwiper();
    setManufactureSwiper();
    setCardSliderSm_Manufacture();
    setCardSliderLg_Manufacture();
    setBuildingSwiper();
    setCardSliderSm_Building();
    setCardSliderLg_Building();
    setCoverflowSwiper_1();
    setCoverflowSwiper_2();
    setLicenseSlider();
};