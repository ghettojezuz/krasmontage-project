import $ from 'jquery';

export const setCapacity = () => {
    const rangeSliders = $('.input-range');
    const rangeSliders_tons = $('.input-suffix__tons');
    const rangeSliders_cost = $('.input-suffix__cost');
    const capacityForms = [...$('.capacity__form')];
    const min = 10;
    const max = 10000;
    const manufactureCost = 18000;
    const buildingCost = 20000;

    capacityForms.forEach((form) => {
        form.addEventListener('submit', (e) => {
            e.preventDefault();
        })
    });

    const setRangeSlider = (rangeSlider, tons, cost, price) => {
        rangeSlider.addEventListener('input', () => {
            tons.value = rangeSlider.value;
            cost.value = rangeSlider.value * price;
        });

        tons.addEventListener('change', () => {
            if (tons.value >= min && tons.value <= max) {
                rangeSlider.value = tons.value;
                cost.value = tons.value * price;
            } else if (tons.value < min) {
                rangeSlider.value = min;
                cost.value = min * price;
                tons.value = min;
            } else if (tons.value > max) {
                rangeSlider.value = max;
                cost.value = max * price;
                tons.value = max;
            }
        });

        cost.addEventListener('change', () => {
            if (cost.value >= (min * price) && cost.value <= (max * price)) {
                let fineValue = Math.trunc(cost.value / price);
                rangeSlider.value = fineValue;
                tons.value = fineValue;
                cost.value = fineValue * price;
            } else if (cost.value < (min * price)) {
                rangeSlider.value = min;
                tons.value = min;
                cost.value = min * price;
            } else if (cost.value > (max * price)) {
                rangeSlider.value = max;
                tons.value = max;
                cost.value = max * price;
            }

        })
    };

    setRangeSlider(rangeSliders[0], rangeSliders_tons[0], rangeSliders_cost[0], manufactureCost);
    setRangeSlider(rangeSliders[1], rangeSliders_tons[1], rangeSliders_cost[1], manufactureCost);
    setRangeSlider(rangeSliders[2], rangeSliders_tons[2], rangeSliders_cost[2], buildingCost);
    setRangeSlider(rangeSliders[3], rangeSliders_tons[3], rangeSliders_cost[3], buildingCost);
    setRangeSlider(rangeSliders[4], rangeSliders_tons[4], rangeSliders_cost[4], buildingCost);
    setRangeSlider(rangeSliders[5], rangeSliders_tons[5], rangeSliders_cost[5], buildingCost);

};